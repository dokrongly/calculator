# Calculator

Using Swift4 and xCode 9.4  
Build a calculator app with two screens  
Screen 1 is the calculator  
Screen 2 shows history of all equations input during current session  
App must have a NavBar  
App must have a Modal popup
