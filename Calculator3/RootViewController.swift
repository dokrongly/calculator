//
//  RootViewController.swift
//  Calculator3
//
//  Created by Intern on 7/5/19.
//  Copyright © 2019 Intern. All rights reserved.
//

import UIKit
var History = ["No history yet.", "Please input some math first."]

class RootViewController: UIViewController, UIPageViewControllerDelegate {

    var pageViewController: UIPageViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        // Configure the page view controller and add it as a child view controller.
        self.pageViewController = UIPageViewController(transitionStyle: .pageCurl, navigationOrientation: .horizontal, options: nil)
        self.pageViewController!.delegate = self
        //Donna's note: this is code that was part of the template.  I should try to understand it
//        let startingViewController: DataViewController = self.modelController.viewControllerAtIndex(0, storyboard: self.storyboard!)!
//        let viewControllers = [startingViewController]
//        self.pageViewController!.setViewControllers(viewControllers, direction: .forward, animated: false, completion: {done in })
//
//        self.pageViewController!.dataSource = self.modelController
//
//        self.addChildViewController(self.pageViewController!)
//        self.view.addSubview(self.pageViewController!.view)
//
//        // Set the page view controller's bounds using an inset rect so that self's view is visible around the edges of the pages.
//        var pageViewRect = self.view.bounds
//        if UIDevice.current.userInterfaceIdiom == .pad {
//            pageViewRect = pageViewRect.insetBy(dx: 40.0, dy: 40.0)
//        }
//        self.pageViewController!.view.frame = pageViewRect
//
//        self.pageViewController!.didMove(toParentViewController: self)
        
        assignbackground()
        navigationController?.navigationBar.shadowImage = UIImage()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewDidAppear(_ animated: Bool) {
        // 1
        let nav = self.navigationController?.navigationBar
        
        // 2
        nav?.barStyle = UIBarStyle.black
        nav?.tintColor = UIColor.yellow

    }

    var modelController: ModelController {
        // Return the model controller object, creating it if necessary.
        // In more complex implementations, the model controller may be passed to the view controller.
        if _modelController == nil {
            _modelController = ModelController()
        }
        return _modelController!
    }

    var _modelController: ModelController? = nil

    // MARK: - UIPageViewController delegate methods

    func pageViewController(_ pageViewController: UIPageViewController, spineLocationFor orientation: UIInterfaceOrientation) -> UIPageViewControllerSpineLocation {
        if (orientation == .portrait) || (orientation == .portraitUpsideDown) || (UIDevice.current.userInterfaceIdiom == .phone) {
            // In portrait orientation or on iPhone: Set the spine position to "min" and the page view controller's view controllers array to contain just one view controller. Setting the spine position to 'UIPageViewControllerSpineLocationMid' in landscape orientation sets the doubleSided property to true, so set it to false here.
            let currentViewController = self.pageViewController!.viewControllers![0]
            let viewControllers = [currentViewController]
            self.pageViewController!.setViewControllers(viewControllers, direction: .forward, animated: true, completion: {done in })

            self.pageViewController!.isDoubleSided = false
            return .min
        }

        // In landscape orientation: Set set the spine location to "mid" and the page view controller's view controllers array to contain two view controllers. If the current page is even, set it to contain the current and next view controllers; if it is odd, set the array to contain the previous and current view controllers.
        let currentViewController = self.pageViewController!.viewControllers![0] as! DataViewController
        var viewControllers: [UIViewController]

        let indexOfCurrentViewController = self.modelController.indexOfViewController(currentViewController)
        if (indexOfCurrentViewController == 0) || (indexOfCurrentViewController % 2 == 0) {
            let nextViewController = self.modelController.pageViewController(self.pageViewController!, viewControllerAfter: currentViewController)
            viewControllers = [currentViewController, nextViewController!]
        } else {
            let previousViewController = self.modelController.pageViewController(self.pageViewController!, viewControllerBefore: currentViewController)
            viewControllers = [previousViewController!, currentViewController]
        }
        self.pageViewController!.setViewControllers(viewControllers, direction: .forward, animated: true, completion: {done in })

        return .mid
    }

    //working variables
    var tempString: String?
    var firstNbr: Double?
    var secondNbr: Double?
    //Donna's Note:
    // I really should have made an array for numbers and
    // another for operators so user can build an equation of more
    // than just two operands
    // But if they mix multiplication/division with addition/subtraction I would have to parse through the equation and determine order or operations -- I'm just too overwhelmed at the moment to deal with the extra level of difficulty
    
    var operation:Int = 0
    var solution: Double = 0
    var Dictionary: [Int:String] = [16:"+", 15:"-", 14:"*", 13:"/"]
    var whatToDisplay: String?
 
    @IBOutlet weak var labelArea: UILabel!
    
    @IBAction func buttons(sender: UIButton){
        
        //number & decimal buttons
        if let inputValue = sender.titleLabel!.text {
            //watch for operator -- until then save input to string
            if sender.tag < 12 {
                //just a plain number or decimal button
                //store value of button into a string that will collect input until an operator is pressed
                
                //safely get value from tempString if there is one yet
                //and add new input to end of tempString
                if let tst = self.tempString {
                    
                    tempString = tst + inputValue
                    //update what is displayed to user
                    if operation == 0 {
                        whatToDisplay = tempString!
                    }
                        
                    else if operation > 0 {
                        if let sdi = self.whatToDisplay {
                            whatToDisplay = sdi + inputValue
                        }
                        else {
                            whatToDisplay = tempString!
                        }
                    }
                }
                    //this might be first input ever and tempString might be empty
                else {
                    tempString = inputValue
                    if let sdi = self.whatToDisplay {
                        whatToDisplay = sdi + inputValue
                    }
                    else {
                        whatToDisplay = tempString!
                    }
                }
                
                //update user display
                labelArea.text = whatToDisplay!
                
            }//end if sender.tag < 12
                
                //non-numeric buttons e.g. operator buttons
            else if sender.tag >= 12 {
                //this is an operator button
                //if it's the C (clear) button, clear something
                //if it's an operator, store the number & update the user display
                //if it's the equal sign, do math and update screen and clear working vars
                
                //clear (C) button
                if sender.tag == 12 {
                    
                    if operation != 0 {
                        if let nwts = self.firstNbr {
                            whatToDisplay = String(nwts) + " " + Dictionary[operation]! + " "
                        }
                        else {whatToDisplay = " "}
                    }
                        
                    else if operation == 0 {
                        if let nwts = self.firstNbr {
                            whatToDisplay = String(nwts) + " "
                        }
                        else {
                            whatToDisplay = " "
                        }
                    }
                    labelArea.text = whatToDisplay!
                    
                    //clear the tempString
                    tempString = nil
                }//end if sender.tag = 12 (clear button)
                    
                    //any other operator other than C button or equal sign
                else if sender.tag > 12 && sender.tag < 17 {
                    //don't bother putting operator into tempString
                    //just store it, then go get tempString and save it as an operand
                    operation = sender.tag
                    
                    //take numeric input we saved as tempString and convert to double and save to its own variable
                    if let n = self.tempString {
                        firstNbr = Double(n)
                        //if there's already something in whatToDisplay it will be firstNbr
                        if let wtd = self.whatToDisplay {
                            whatToDisplay = wtd + " " + Dictionary[operation]! + " "
                        }
                            //if whatToDisplay is empty so far
                        else {
                            whatToDisplay = n + " " + Dictionary[operation]! + " "
                        }
                    }
                        
                        //just in case tempString is empty hard code firstNbr
                    else {
                        firstNbr = 0
                        whatToDisplay = "0 " + Dictionary[operation]! + " "
                    }
                    
                    //update user display
                    labelArea.text = whatToDisplay!
                    
                    //clear the tempString
                    tempString = nil
                }//end if sender.tag > 12 && < 17
                    
                    //if button is the equal key, time to do math
                else if (sender.tag == 17){
                    //get the tempString which contains user's final number
                    //and store it
                    if let n = self.tempString {
                        secondNbr = Double(n)
                    }
                        //if tempString is empty hard code secondNbr
                    else {
                        secondNbr = 0
                    }
                    //if whatToDisplay contains something it will be all numbers, only missing equal sign
                    if let fwtd = self.whatToDisplay {
                        whatToDisplay = fwtd + " = "
                        labelArea.text = whatToDisplay!
                    }
                    
                    //get the numbers we've stored
                    if let num1 = self.firstNbr {
                        firstNbr = num1
                    }
                    else {firstNbr = 0}
                    
                    if let num2 = self.secondNbr {
                        secondNbr = num2
                    }
                    else {secondNbr = 0}
                    
                    //here's the math
                    if(operation == 16){ solution = firstNbr! + secondNbr! }
                    else if(operation == 15){solution = firstNbr! - secondNbr!}
                    else if(operation == 14){solution = firstNbr! * secondNbr!}
                    else if(operation == 13){solution = firstNbr! / secondNbr!}
                    
                    //solution must also be in string form to display
                    let solutionToDisplay = String(format: "%.2f", ceil(firstNbr!*100)/100) + " " + Dictionary[operation]! + " " + String(format: "%.2f", ceil(secondNbr!*100)/100) + " = " + String(format: "%.2f", ceil(solution*100)/100)
                    
                    if let fwtd = self.whatToDisplay {
                        whatToDisplay = solutionToDisplay
                        print("\(fwtd)")
                    }
                    
                    //send complete equation to display
                    labelArea.text = whatToDisplay!
                    
                    //save this equation to history array
                    if History[0] == "No history yet." {
                        History[0] = whatToDisplay!
                        History[1] = "" 
                    }
                    else {
                        History.append(whatToDisplay!)
                    }
                    for row in History {
                        print("\(row)")
                    }
                    
                    //then clear all the working variables
                    tempString = nil
                    whatToDisplay = nil
                    firstNbr = nil
                    secondNbr = nil
                    operation = 0
                    solution = 0
                    
                }//end else if sender.tag = 17
                
            }//end if sender.tag >= 12
            
        }//end if there is an inputValue
        
    }//end IBAction
    
    func assignbackground(){
        let background = UIImage(named: "background")
        
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIViewContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubview(toBack: imageView)
    }
}

