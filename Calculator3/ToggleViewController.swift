//
//  ToggleViewController.swift
//  Calculator3
//
//  Created by Intern on 7/7/19.
//  Copyright © 2019 Intern. All rights reserved.
//

import UIKit

class ToggleViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func dismissPopup(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clearHistory() {
        History = ["No history yet.", "Please input some math first."]
        dismiss(animated: true, completion: nil)
        print("\(History)")
        
    }

}
